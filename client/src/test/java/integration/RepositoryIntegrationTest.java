package integration;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.ilinovsg.tm.client.generated.Role;
import ru.ilinovsg.tm.client.model.User;
import ru.ilinovsg.tm.client.repository.UserSoapRepository;
import ru.ilinovsg.tm.client.repository.impl.UserSoapRepositoryImpl;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RepositoryIntegrationTest {
    private static final Long USER_ID = 1L;
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String FIRSTNAME = "firstName";
    private static final String LASTNAME = "lastName";
    private static final Role ROLE = Role.USER;
    private UserSoapRepository userSoapRepository;

    @BeforeEach
    void setUp() {
        userSoapRepository = new UserSoapRepositoryImpl();
    }

    @Test
    void testCreateUser() {
        User user = User.builder().login(LOGIN).password(PASSWORD).firstName(FIRSTNAME).lastName(LASTNAME).role(ROLE).build();
        Integer id = userSoapRepository.createUser(user);
        User responseUser = userSoapRepository.findById(id.longValue());
        assertEquals(user.getLogin(), responseUser.getFirstName());
    }
}
