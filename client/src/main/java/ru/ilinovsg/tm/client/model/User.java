package ru.ilinovsg.tm.client.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import ru.ilinovsg.tm.client.generated.Role;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private Long id;
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private Role role;
}
