package ru.ilinovsg.tm.client.repository.mapper;

import org.mapstruct.Mapper;
import ru.ilinovsg.tm.client.generated.UserDTO;
import ru.ilinovsg.tm.client.model.User;

import java.util.List;

@Mapper
public abstract class UserMapper {
    public abstract UserDTO userToUserDTO(User user);

    public abstract User userDTOToUser(UserDTO userDTO);

    public abstract List<User> userListDTOToUser(List<UserDTO> userDTO);
}