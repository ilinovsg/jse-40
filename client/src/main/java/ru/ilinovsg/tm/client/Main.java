package ru.ilinovsg.tm.client;

import lombok.extern.log4j.Log4j2;
import ru.ilinovsg.tm.client.generated.*;

@Log4j2
public class Main {
    public static void main(String[] args) {
        UserControllerImplService service = new UserControllerImplService();
        UserController controllerImplPort = service.getUserControllerImplPort();

        UserDTO userDTO = new UserDTO();
/*
        userDTO.setAge(12);
        userDTO.setName("sdfsdf");
        userDTO.setSurname("dfgdfgdg");
        userDTO.setCity("gggg");
*/
        UserListResponseDTO responseDTO = controllerImplPort.getAllUsers();
        if (responseDTO.getStatus() == Status.OK) {
            log.debug("resp = {}", responseDTO.getPayload());
        }
    }
}
