package ru.ilinovsg.tm.service;

import ru.ilinovsg.tm.dto.ProjectDTO;
import ru.ilinovsg.tm.dto.ProjectListResponseDTO;
import ru.ilinovsg.tm.dto.ProjectResponseDTO;

public interface ProjectService {
    ProjectResponseDTO createProject(ProjectDTO projectDTO);

    ProjectResponseDTO updateProject(ProjectDTO projectDTO);

    ProjectResponseDTO deleteProject(Long id);

    ProjectResponseDTO getProject(Long id);

    ProjectListResponseDTO getAllProjects();

    ProjectListResponseDTO findByName(String name);
}
