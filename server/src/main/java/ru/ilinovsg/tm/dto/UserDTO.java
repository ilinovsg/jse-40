package ru.ilinovsg.tm.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import ru.ilinovsg.tm.enumerated.Role;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO implements Serializable {
    public static final Long serialVersionUID = 1L;

    private Long id;
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private Role role;
}
