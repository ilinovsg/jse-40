package ru.ilinovsg.tm.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProjectDTO implements Serializable {
    public static final Long serialVersionUID = 1L;

    private Long id;
    private String name = "";
    private String description = "";
    private Long userId;
}
