package ru.ilinovsg.tm.controller.impl;

import lombok.extern.log4j.Log4j2;
import ru.ilinovsg.tm.controller.UserController;
import ru.ilinovsg.tm.dto.UserDTO;
import ru.ilinovsg.tm.dto.UserListResponseDTO;
import ru.ilinovsg.tm.dto.UserResponseDTO;
import ru.ilinovsg.tm.service.UserService;

import javax.jws.WebService;

@Log4j2
@WebService(endpointInterface = "ru.ilinovsg.tm.controller.UserController")
public class UserControllerImpl implements UserController {
    private UserService userService;

    public UserControllerImpl() {
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserResponseDTO createUser(UserDTO userDTO) {
        return userService.createUser(userDTO);
    }

    @Override
    public UserResponseDTO updateUser(UserDTO userDTO) {
        return userService.updateUser(userDTO);
    }

    @Override
    public UserResponseDTO deleteUser(Long id) {
        return userService.deleteUser(id);
    }

    @Override
    public UserResponseDTO getUser(Long id) {
        return userService.getUser(id);
    }

    @Override
    public UserListResponseDTO getAllUsers() {
        return userService.getAllUsers();
    }

    @Override
    public UserListResponseDTO findByName(String name) {
        return userService.findByName(name);
    }
}
