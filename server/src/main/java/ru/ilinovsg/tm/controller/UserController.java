package ru.ilinovsg.tm.controller;

import ru.ilinovsg.tm.dto.UserDTO;
import ru.ilinovsg.tm.dto.UserListResponseDTO;
import ru.ilinovsg.tm.dto.UserResponseDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface UserController {
    @WebMethod
    UserResponseDTO createUser(UserDTO userDTO);

    @WebMethod
    UserResponseDTO updateUser(UserDTO userDTO);

    @WebMethod
    UserResponseDTO deleteUser(Long id);

    @WebMethod
    UserResponseDTO getUser(Long id);

    @WebMethod
    UserListResponseDTO getAllUsers();

    @WebMethod
    UserListResponseDTO findByName(String name);
}
