package ru.ilinovsg.tm.controller;

import ru.ilinovsg.tm.dto.ProjectDTO;
import ru.ilinovsg.tm.dto.ProjectListResponseDTO;
import ru.ilinovsg.tm.dto.ProjectResponseDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface ProjectController {
    @WebMethod
    ProjectResponseDTO createProject(ProjectDTO projectDTO);

    @WebMethod
    ProjectResponseDTO updateProject(ProjectDTO projectDTO);

    @WebMethod
    ProjectResponseDTO deleteProject(Long id);

    @WebMethod
    ProjectResponseDTO getProject(Long id);

    @WebMethod
    ProjectListResponseDTO getAllProjects();

    @WebMethod
    ProjectListResponseDTO findByName(String name);
}
