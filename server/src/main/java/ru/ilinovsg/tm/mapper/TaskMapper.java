package ru.ilinovsg.tm.mapper;

import ru.ilinovsg.tm.dto.TaskDTO;
import ru.ilinovsg.tm.entity.Task;

public class TaskMapper {
    public static TaskDTO toDto(Task task) {
        return TaskDTO.builder()
                .id(task.getId())
                .name(task.getName())
                .description(task.getDescription())
                .userId(task.getUserId())
                .projectId(task.getProjectId())
                .build();
    }
}
