package ru.ilinovsg.tm.mapper;

import ru.ilinovsg.tm.dto.ProjectDTO;
import ru.ilinovsg.tm.entity.Project;


public class ProjectMapper {
    public static ProjectDTO toDto(Project project) {
        return ProjectDTO.builder()
                .id(project.getId())
                .name(project.getName())
                .description(project.getDescription())
                .userId(project.getUserId())
                .build();
    }
}
