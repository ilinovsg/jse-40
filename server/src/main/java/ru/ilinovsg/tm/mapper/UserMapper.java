package ru.ilinovsg.tm.mapper;

import ru.ilinovsg.tm.dto.UserDTO;
import ru.ilinovsg.tm.entity.User;

public class UserMapper {
    public static UserDTO toDto(User user) {
        return UserDTO.builder()
                .id(user.getId())
                .login(user.getLogin())
                .password(user.getPassword())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .role(user.getRole())
                .build();
    }
}
