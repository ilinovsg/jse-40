package ru.ilinovsg.tm.repository.impl;

import ru.ilinovsg.tm.repository.ProjectRepository;
import ru.ilinovsg.tm.entity.Project;
import ru.ilinovsg.tm.repository.AbstractRepository;

public class ProjectRepositoryImpl extends AbstractRepository<Project> implements ProjectRepository {

    private ProjectRepositoryImpl() {
    }

    private static volatile ProjectRepositoryImpl instance = null;

    public static ProjectRepositoryImpl getInstance(){
        synchronized (ProjectRepositoryImpl.class) {
            if (instance == null) {
                instance = new ProjectRepositoryImpl();
            }
        }
        return instance;
    }
}
