package ru.ilinovsg.tm.repository;

import ru.ilinovsg.tm.entity.Task;

public interface TaskRepository extends Repository<Task>{
}
