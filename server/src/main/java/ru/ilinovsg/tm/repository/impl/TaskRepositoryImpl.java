package ru.ilinovsg.tm.repository.impl;

import ru.ilinovsg.tm.repository.AbstractRepository;
import ru.ilinovsg.tm.repository.TaskRepository;
import ru.ilinovsg.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TaskRepositoryImpl extends AbstractRepository<Task> implements TaskRepository {

    private TaskRepositoryImpl() {
    }

    private static TaskRepositoryImpl instance = null;

    public static TaskRepositoryImpl getInstance(){
        synchronized (TaskRepositoryImpl.class) {
            if (instance == null) {
                instance = new TaskRepositoryImpl();
            }
        }
        return instance;
    }

    public List<Task> findAllByProjectId(final Long projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (idProject.equals(projectId)) result.add(task);
        }
        return result;
    }

    public Optional<Task> findByProjectIdAndId(final Long projectId, final Long id) {
        for (final Task task : findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (!idProject.equals(projectId)) continue;
            if (task.getId().equals(id)) return Optional.of(task);
        }
        return Optional.empty();
    }

    public Optional<Task> removeByProjectId(final Long projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (idProject.equals(projectId)) result.remove(task);
        }
        return Optional.empty();
    }
}
