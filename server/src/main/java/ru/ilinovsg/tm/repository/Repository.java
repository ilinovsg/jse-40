package ru.ilinovsg.tm.repository;

import ru.ilinovsg.tm.entity.AbstractEntity;

import java.util.List;
import java.util.Optional;

public interface Repository<E extends AbstractEntity> {
    Optional<E> create(E input);

    Optional<E> update(E input);

    Optional<E> findById(Long id);

    void delete(Long id);

    List<E> findAll();
}
