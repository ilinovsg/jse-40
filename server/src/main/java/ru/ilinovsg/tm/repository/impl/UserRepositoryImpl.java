package ru.ilinovsg.tm.repository.impl;

import ru.ilinovsg.tm.repository.AbstractRepository;
import ru.ilinovsg.tm.repository.UserRepository;
import ru.ilinovsg.tm.entity.User;
import ru.ilinovsg.tm.enumerated.Role;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserRepositoryImpl extends AbstractRepository<User> implements UserRepository {

    private UserRepositoryImpl() {
    }

    private static volatile UserRepositoryImpl instance = null;

    public static UserRepositoryImpl getInstance(){
        synchronized (UserRepositoryImpl.class) {
            if (instance == null) {
                instance = new UserRepositoryImpl();
            }
        }
        return instance;
    }

    private final List<User> users = new ArrayList<>();

    public User create(final String login, final String password, final String firstName, final String lastName, final Role role) {
        final User user = new User();
        user.setLogin(login);
        user.setPassword(password);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setRole(role);
        users.add(user);
        return user;
    }

    public Optional<User> update(final Long id, final String login, final String password, final String firstName, final String lastName, final Role role) {
        final Optional<User> user = findById(id);
        if (user.isPresent()) {
            user.get().setId(id);
            user.get().setLogin(login);
            user.get().setPassword(password);
            user.get().setFirstName(firstName);
            user.get().setLastName(lastName);
            user.get().setRole(role);
            return user;
        } else {
            return Optional.empty();
        }
    }

    public Optional<User> update(final Long id, final String password) {
        final Optional<User> user = findById(id);
        if (user.isPresent()) {
            user.get().setId(id);
            user.get().setPassword(password);
            return user;
        } else {
            return Optional.empty();
        }
    }

    public Optional<User> findById(final Long id) {
        for (final User user : users) {
            if (user.getId().equals(id)) return Optional.of(user);
        }
        return Optional.empty();
    }

    public Optional<User> findByLogin(final String login) {
        for (final User user : users) {
            if (user.getLogin().equals(login)) return Optional.of(user);
        }
        return Optional.empty();
    }

    public Optional<User> removeById(final Long id) {
        final Optional<User> user = findById(id);
        if (user.isPresent()) {
            users.remove(user.get());
            return user;
        } else {
            return Optional.empty();
        }
    }

    public Optional<User> removeByLogin(final String login) {
        final Optional<User> user = findByLogin(login);
        if (user.isPresent()) {
            users.remove(user.get());
            return user;
        } else {
            return Optional.empty();
        }
    }

    public void clear() { users.clear(); }

    public List<User> findAll() { return users; }
}
