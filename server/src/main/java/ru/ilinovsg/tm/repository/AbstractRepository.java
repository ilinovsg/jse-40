package ru.ilinovsg.tm.repository;

import ru.ilinovsg.tm.entity.AbstractEntity;

import java.util.*;

public class AbstractRepository<E extends AbstractEntity> implements Repository<E>{
    private final Map<Long, E> storage = new HashMap<>();

    @Override
    public Optional<E> create(E value) {
        synchronized (storage) {
            Long id = findNewKey();
            value.setId(id);
            storage.put(id, value);
        }
        return Optional.ofNullable(value);
    }

    private Long findNewKey() {
        if (storage.size() > 0) {
            return Collections.max(storage.keySet()) + 1L;
        }
        return 1L;
    }

    @Override
    public Optional<E> update(E value) {
        storage.put(value.getId(), value);
        return Optional.of(value);
    }

    @Override
    public Optional<E> findById(Long id) {
        return Optional.ofNullable(storage.get(id));
    }

    @Override
    public void delete(Long id) {
        storage.remove(id);
    }

    @Override
    public List<E> findAll() {
        return new ArrayList<>(storage.values());
    }

/*

    public Optional<E> findByIndex(int index) {
        return Optional.of(items.get(index));
    }

    public Optional<E> removeByIndex(final int index) {
        final Optional<E> result = findByIndex(index);
        if (result.isPresent()) {
            items.remove(result);
            itemHashMap.remove(result.get().getName());
            return result;
        } else {
            return Optional.empty();
        }
    }

    public Optional<E> findByName(final String name) {
        if (itemHashMap.containsKey(name)) {
            AbstractEntity result = itemHashMap.get(name);
            return Optional.of((E) result);
        }
        else return Optional.empty();
    }

    public Optional<E> removeByName(final String name) {
        final Optional<E> result = findByName(name);
        if (result.isPresent()) {
            items.remove(result);
            itemHashMap.remove(result.get().getName());
            return result;
        } else {
            return Optional.empty();
        }
    }

    public Optional<E> findById(final Long id) {
        Optional<E> result = Optional.empty();
        for (final E item : items) {
            if (id.equals(item.getId())) {
                result = Optional.of(item);
                break;
            }
        }
        return result;
    }

    public Optional<E> removeById(final Long id) {
        final Optional<E> result = findById(id);
        result.ifPresent(val -> {
            items.remove(result.get());
            itemHashMap.remove(result.get().getName());
        });
        return result;
    }

    public List<E> findAllByUserId(final Long userId) {
        final List<E> items = new ArrayList<>();
        for (final E result : findAll()) {
            final Long idUser = result.getUserId();
            if (idUser == null) continue;
            if (idUser.equals(userId)) items.add((E) result);
        }
        return items;
    }

    public void clear() {
        items.clear();
    }

    public List<E> findAll() {
        return items;
    }

    public int getSize() {
        return items.size();
    }

    public Optional<E> findByUserIdAndId(final Long userId, final Long id) {
        for (final E result : items) {
            final Long idUser = result.getUserId();
            if (idUser == null) continue;
            if (!idUser.equals(userId)) continue;
            if (result.getId().equals(id)) return Optional.of(result);
        }
        return Optional.empty();
    }
*/
}
