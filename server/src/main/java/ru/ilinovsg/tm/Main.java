package ru.ilinovsg.tm;

import ru.ilinovsg.tm.controller.impl.TaskControllerImpl;
import ru.ilinovsg.tm.controller.impl.UserControllerImpl;
import ru.ilinovsg.tm.repository.impl.ProjectRepositoryImpl;
import ru.ilinovsg.tm.repository.impl.TaskRepositoryImpl;
import ru.ilinovsg.tm.repository.impl.UserRepositoryImpl;
import ru.ilinovsg.tm.service.impl.ProjectServiceImpl;
import ru.ilinovsg.tm.service.impl.TaskServiceImpl;
import ru.ilinovsg.tm.service.impl.UserServiceImpl;
import ru.ilinovsg.tm.controller.impl.ProjectControllerImpl;

import javax.xml.ws.Endpoint;

public class Main {
    public static void main(final String[] args) {
        UserControllerImpl userController = new UserControllerImpl();
        userController.setUserService(UserServiceImpl.getInstance(UserRepositoryImpl.getInstance()));
        Endpoint.publish("http://localhost:8899/ws/user", userController);

        ProjectControllerImpl projectController = new ProjectControllerImpl();
        projectController.setProjectService(ProjectServiceImpl.getInstance(ProjectRepositoryImpl.getInstance()));
        Endpoint.publish("http://localhost:8899/ws/project", projectController);

        TaskControllerImpl taskController = new TaskControllerImpl();
        taskController.setTaskService(TaskServiceImpl.getInstance(TaskRepositoryImpl.getInstance()));
        Endpoint.publish("http://localhost:8899/ws/task", taskController);

    }
}
