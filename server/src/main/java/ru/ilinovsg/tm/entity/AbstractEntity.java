package ru.ilinovsg.tm.entity;

import lombok.Getter;
import lombok.Setter;

public abstract class AbstractEntity {
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String name = "";

    @Getter
    @Setter
    private String description = "";

    @Getter
    @Setter
    private Long userId;
}
