package ru.ilinovsg.tm.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Task extends AbstractEntity {
    private Long id;
    private String name = "";
    private String description = "";
    private Long userId;
    private Long projectId;
    private LocalDateTime deadline;
}
