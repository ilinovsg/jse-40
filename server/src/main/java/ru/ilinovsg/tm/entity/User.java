package ru.ilinovsg.tm.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.ilinovsg.tm.enumerated.Role;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User extends AbstractEntity{

    private Long id;
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private Role role;
}
