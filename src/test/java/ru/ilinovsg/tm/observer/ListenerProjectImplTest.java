package ru.ilinovsg.tm.observer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.ilinovsg.tm.service.ProjectService;

import static org.junit.jupiter.api.Assertions.*;

class ListenerProjectImplTest {
    private ProjectService projectService;

    @BeforeEach
    void setUp() {
        projectService = ProjectService.getInstance();
        projectService.create("PROJECT 3", "test");
    }

    @Test
    void update() {
        assertEquals(0, projectService.create("pr1", "test"));
    }
}