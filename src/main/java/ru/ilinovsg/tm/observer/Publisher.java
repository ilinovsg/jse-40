package ru.ilinovsg.tm.observer;

import ru.ilinovsg.tm.exception.ProjectNotFoundException;
import ru.ilinovsg.tm.exception.TaskNotFoundException;

public interface Publisher {
    void addListener(Listener listner);
    void deleteListener(Listener listner);
    void notify(String command) throws ProjectNotFoundException, TaskNotFoundException;
    void start();
}
