package ru.ilinovsg.tm.observer;

import ru.ilinovsg.tm.exception.ProjectNotFoundException;
import ru.ilinovsg.tm.service.ProjectService;

import static ru.ilinovsg.tm.constant.TerminalConst.*;

public class ListenerProjectImpl implements Listener{
    @Override
    public int update(String command) throws ProjectNotFoundException {
        ProjectService projectService = ProjectService.getInstance();

        switch (command) {
            case PROJECT_CREATE:
                return projectService.createProject();
            case PROJECT_REMOVE_BY_NAME:
                return projectService.removeProjectByName();
            case PROJECT_REMOVE_BY_ID:
                return projectService.removeProjectById();
            case PROJECT_REMOVE_BY_INDEX:
                return projectService.removeProjectByIndex();
            case PROJECT_UPDATE_BY_NAME:
                return projectService.updateProjectByName();
            case PROJECT_UPDATE_BY_ID:
                return projectService.updateProjectById();
            case PROJECT_UPDATE_BY_INDEX:
                return projectService.updateProjectByIndex();
            case PROJECT_CLEAR:
                return projectService.clearProject();
            case PROJECT_LIST:
                return projectService.listProject();
            case PROJECT_VIEW_BY_ID:
                return projectService.viewProjectById();
            case PROJECT_VIEW_BY_INDEX:
                return projectService.viewProjectByIndex();
            case PROJECT_VIEW_BY_NAME:
                return projectService.viewProjectByName();
            case PROJECT_LIST_BY_USER_ID:
                return projectService.listProjectByUserId();
            case PROJECT_ADD_TO_USER_BY_IDS:
                return projectService.addProjectToUserByIds();
            case PROJECT_REMOVE_FROM_USER_BY_IDS:
                return projectService.removeProjectFromUserByIds();
        }
        return -1;
    }
}
