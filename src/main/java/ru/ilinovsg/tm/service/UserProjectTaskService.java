package ru.ilinovsg.tm.service;

import ru.ilinovsg.tm.entity.Project;
import ru.ilinovsg.tm.entity.Task;
import ru.ilinovsg.tm.exception.ProjectNotFoundException;
import ru.ilinovsg.tm.exception.TaskNotFoundException;
import ru.ilinovsg.tm.repository.ProjectRepository;
import ru.ilinovsg.tm.repository.TaskRepository;
import ru.ilinovsg.tm.repository.UserRepository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class UserProjectTaskService {

    private UserProjectTaskService() {
    }

    private static UserProjectTaskService instance = null;

    public static UserProjectTaskService getInstance(){
        synchronized (UserProjectTaskService.class) {
            if (instance == null) {
                instance = new UserProjectTaskService();
            }
        }
        return instance;
    }

    UserRepository userRepository = UserRepository.getInstance();

    ProjectRepository projectRepository = ProjectRepository.getInstance();

    TaskRepository taskRepository = TaskRepository.getInstance();

    public UserProjectTaskService(UserRepository userRepository, ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.userRepository = userRepository;
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public List<Project> findAllProjectsByUserId(final Long userId) {
        if (userId == null) return Collections.emptyList();
        return projectRepository.findAllByUserId(userId);
    }

    public List<Task> findAllTasksByUserId(final Long userId) {
        if (userId == null) return Collections.emptyList();
        return taskRepository.findAllByUserId(userId);
    }

    public Optional<Task> addTaskToUser (final Long userId, final Long taskId) throws TaskNotFoundException {
        //final Optional<User> user = userRepository.findById(userId);
        final Optional<Task> task = taskRepository.findById(taskId);
        task.ifPresent(value -> value.setUserId(userId));
        return task;
    }

    public Optional<Task> removeTaskFromUser (final Long userId, final Long taskId) {
        final Optional<Task> task = taskRepository.findByUserIdAndId(userId, taskId);
        task.ifPresent(value -> value.setUserId(null));
        return task;
    }

    public Optional<Project> addProjectToUser (final Long userId, final Long projectId) throws ProjectNotFoundException {
        //final Optional<User> user = userRepository.findById(userId);
        final Optional<Project> project = projectRepository.findById(projectId);
        project.ifPresent(value -> value.setUserId(userId));
        return project;
    }

    public Optional<Project> removeProjectFromUser (final Long userId, final Long projectId) {
        final Optional<Project> project = projectRepository.findByUserIdAndId(userId, projectId);
        project.ifPresent(value -> value.setUserId(null));
        return project;
    }
}
