package ru.ilinovsg.tm.entity;

import java.time.LocalDateTime;

public class Task extends AbstractEntity {

    private Long projectId;

    private LocalDateTime deadline;

    public Task() {
    }

    public Task(String name) {
        super(name);
    }

    public Task(Long id, String name, String description, Long userId, Long projectId, LocalDateTime deadline) {
        super(id, name, description, userId);
        this.projectId = projectId;
        this.deadline = deadline;
    }

    public Task(String name, String description, LocalDateTime deadline) {
        super(name, description);
        this.deadline = deadline;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public LocalDateTime getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDateTime deadline) {
        this.deadline = deadline;
    }
}
