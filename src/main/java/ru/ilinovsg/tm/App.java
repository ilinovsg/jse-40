package ru.ilinovsg.tm;

import org.apache.commons.lang3.time.DurationFormatUtils;
import ru.ilinovsg.tm.entity.Project;
import ru.ilinovsg.tm.entity.Task;
import ru.ilinovsg.tm.enumerated.Role;
import ru.ilinovsg.tm.exception.TaskNotFoundException;
import ru.ilinovsg.tm.observer.*;
import ru.ilinovsg.tm.repository.ProjectRepository;
import ru.ilinovsg.tm.repository.TaskRepository;
import ru.ilinovsg.tm.repository.UserRepository;
import ru.ilinovsg.tm.service.*;
import ru.ilinovsg.tm.utils.hashMD5;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class App {
    ProjectRepository projectRepository = ProjectRepository.getInstance();
    ProjectService projectService = ProjectService.getInstance();
    TaskRepository taskRepository = TaskRepository.getInstance();
    static TaskService taskService = TaskService.getInstance();
    UserRepository userRepository = UserRepository.getInstance();
    ProjectTaskService projectTaskService = ProjectTaskService.getInstance();

    {
        projectRepository.create(new Project("PROJECT 3"));
        projectRepository.create(new Project("PROJECT 1"));
        projectRepository.create(new Project("PROJECT 2"));

        taskRepository.create(new Task("TASK 4", "t4", LocalDateTime.now().plusMinutes(1)));
        taskRepository.create(new Task("TASK 5", "t5", LocalDateTime.now().minusMinutes(7)));

        userRepository.create("ADMIN", hashMD5.md5("123QWE"), "Ivan", "Ivanov", Role.Admin);
        userRepository.create("TEST", hashMD5.md5("123"), "Peter", "Petrov", Role.User);
    }

    public static void main(final String[] args) {
        new App();
        displayWelcome();
        runDeadLine();
        Publisher publisher = new PublisherImpl();
        ListenerProjectImpl listenerProject = new  ListenerProjectImpl();
        ListernerTaskImpl listenerTask = new ListernerTaskImpl();
        ListenerSystemImpl listenerSystem = new ListenerSystemImpl();
        ListenerUserImpl listenerUser = new ListenerUserImpl();
        publisher.addListener(listenerProject);
        publisher.addListener(listenerTask);
        publisher.addListener(listenerSystem);
        publisher.addListener(listenerUser);
        publisher.start();
    }

    public static void displayWelcome() {
        System.out.println("***WELCOME TO TASK-MANAGER***");
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public ProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    public static void runDeadLine() {
        LocalDateTime maxDeadline = LocalDateTime.now();
        for (final Task task : taskService.findAll()) {
            if (Duration.between(task.getDeadline(), maxDeadline).toMillis() < 0)
                maxDeadline = task.getDeadline();
        }
        //System.out.println("MaxDeadLine " + maxDeadline);
        LocalDateTime finalMaxDeadline = maxDeadline;
        CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> {
            while (Duration.between(finalMaxDeadline, LocalDateTime.now()).toMillis() < 0) {
                //System.out.println("Time now"+ LocalDateTime.now());
                for (final Task task : taskService.findAll()) {
                    if (Duration.between(task.getDeadline(), LocalDateTime.now()).toMillis() < 0 &&
                        Duration.between(LocalDateTime.now(), task.getDeadline()).toMinutes() > 239) {
                        System.out.println(task.getName() + " be ending after 4 hours");
                    }
                    if (Duration.between(task.getDeadline(), LocalDateTime.now()).toMillis() < 0 &&
                        Duration.between(LocalDateTime.now(), task.getDeadline()).toMinutes() > 59 &&
                        Duration.between(LocalDateTime.now(), task.getDeadline()).toMinutes() < 61) {
                        System.out.println(task.getName() + " be ending after 1 hour");
                    }
                    if (Duration.between(task.getDeadline(), LocalDateTime.now()).toMillis() < 0 &&
                        Duration.between(LocalDateTime.now(), task.getDeadline()).toMinutes() > 29 &&
                        Duration.between(LocalDateTime.now(), task.getDeadline()).toMinutes() < 31) {
                        System.out.println(task.getName() + " be ending after 30 minutes");
                    }
                    if (Duration.between(task.getDeadline(), LocalDateTime.now()).toMillis() < 0 &&
                        Duration.between(LocalDateTime.now(), task.getDeadline()).toMinutes() > 14 &&
                        Duration.between(LocalDateTime.now(), task.getDeadline()).toMinutes() < 16) {
                        System.out.println(task.getName() + " be ending after 15 minutes");
                    }
                    if (Duration.between(task.getDeadline(), LocalDateTime.now()).toMillis() < 0 &&
                        Duration.between(LocalDateTime.now(), task.getDeadline()).toMinutes() > 4 &&
                        Duration.between(LocalDateTime.now(), task.getDeadline()).toMinutes() < 6) {
                        System.out.println(task.getName() + " be ending after 5 minutes");
                    }
                }
                try {
                    TimeUnit.SECONDS.sleep(60);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return "";
        });

        future.thenAccept(result -> {
            try {
                List<Task> tasks = new ArrayList<>();
                for (final Task task : taskService.findAll()) {
                    if (Duration.between(task.getDeadline(), LocalDateTime.now()).toMillis() > 0)
                        tasks.add(task);
                }
                for (final Task task : tasks) {
                    taskService.removeById(task.getId());
                }
            } catch (TaskNotFoundException e) {
                e.printStackTrace();
            }
        });
    }
}
