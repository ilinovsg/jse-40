package ru.ilinovsg.tm.repository;

import ru.ilinovsg.tm.entity.Project;

public class ProjectRepository extends AbstractRepository<Project>{

    private ProjectRepository() {
    }

    private static ProjectRepository instance = null;

    public static ProjectRepository getInstance(){
        synchronized (ProjectRepository.class) {
            if (instance == null) {
                instance = new ProjectRepository();
            }
        }
        return instance;
    }
}
