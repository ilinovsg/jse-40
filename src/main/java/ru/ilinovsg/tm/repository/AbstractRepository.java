package ru.ilinovsg.tm.repository;

import ru.ilinovsg.tm.entity.AbstractEntity;
import ru.ilinovsg.tm.entity.Project;
import ru.ilinovsg.tm.entity.Task;

import java.util.*;

public abstract class AbstractRepository <E extends AbstractEntity>{
    private List<E> items = new ArrayList<>();
    private final Map<String, E> itemHashMap = new HashMap<>();

    public void create(E value) {
        items.add(value);
        itemHashMap.put(value.getName(), value);
    }

    public Optional<E> update(final Long id, final String name) {
        final Optional<E> result = findById(id);
        if (result.isPresent()) {
            result.get().setId(id);
            result.get().setName(name);
            return result;
        } else {
            return Optional.empty();
        }
    }

    public Optional<E> update(final Long id, final String name, final String description) {
        final Optional<E> result = findById(id);
        if (result.isPresent()) {
            result.get().setId(id);
            result.get().setName(name);
            result.get().setDescription(description);
            return result;
        } else {
            return Optional.empty();
        }
    }

    public Optional<E> findByIndex(int index) {
        return Optional.of(items.get(index));
    }

    public Optional<E> removeByIndex(final int index) {
        final Optional<E> result = findByIndex(index);
        if (result.isPresent()) {
            items.remove(result);
            itemHashMap.remove(result.get().getName());
            return result;
        } else {
            return Optional.empty();
        }
    }

    public Optional<E> findByName(final String name) {
        if (itemHashMap.containsKey(name)) {
            AbstractEntity result = itemHashMap.get(name);
            return Optional.of((E) result);
        }
        else return Optional.empty();
    }

    public Optional<E> removeByName(final String name) {
        final Optional<E> result = findByName(name);
        if (result.isPresent()) {
            items.remove(result);
            itemHashMap.remove(result.get().getName());
            return result;
        } else {
            return Optional.empty();
        }
    }

    public Optional<E> findById(final Long id) {
        Optional<E> result = Optional.empty();
        for (final E item : items) {
            if (id.equals(item.getId())) {
                result = Optional.of(item);
                break;
            }
        }
        return result;
    }

    public Optional<E> removeById(final Long id) {
        final Optional<E> result = findById(id);
        result.ifPresent(val -> {
            items.remove(result.get());
            itemHashMap.remove(result.get().getName());
        });
        return result;
    }

    public List<E> findAllByUserId(final Long userId) {
        final List<E> items = new ArrayList<>();
        for (final E result : findAll()) {
            final Long idUser = result.getUserId();
            if (idUser == null) continue;
            if (idUser.equals(userId)) items.add((E) result);
        }
        return items;
    }

    public void clear() {
        items.clear();
    }

    public List<E> findAll() {
        return items;
    }

    public int getSize() {
        return items.size();
    }

    public Optional<E> findByUserIdAndId(final Long userId, final Long id) {
        for (final E result : items) {
            final Long idUser = result.getUserId();
            if (idUser == null) continue;
            if (!idUser.equals(userId)) continue;
            if (result.getId().equals(id)) return Optional.of(result);
        }
        return Optional.empty();
    }
}
